#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <QtWidgets>
#include <QMessageBox>
#include <random>
#include <algorithm>
#include <vector>
#include<QVector>
#include<QPointF>

using namespace std;

typedef struct {
	float x, y, z;
} BOD;

typedef struct {
	float a, b, c;
}TROJUHOLNIK;

class PaintWidget : public QWidget
{
	Q_OBJECT

public:
	vector<BOD> DajBodElipsy() { return bodElipsy; }
	vector<TROJUHOLNIK> DajBodTrojuholnika() { return bodTrojuholnika; }
	int DajXs() { return xs; }
	int DajYs() { return ys; }
	void BezieroveKrivky();

	void DDA(int x1, int y1, int x2, int y2);
	int DajVelkost() { return velkost; }
	void elipsoid(int rovnobezky, int poludniky, int a, int b, int c);
	PaintWidget(QWidget *parent = 0);
	bool openImage(const QString &fileName);
	bool newImage(int x, int y);
	bool saveImage(const QString &fileName);
	void setPenColor(const QColor &newColor);
	void setPenWidth(int newWidth);
	bool isModified() const { return modified; }
	QColor penColor() const { return myPenColor; }
	int penWidth() const { return myPenWidth; }

public slots:
	void clearImage();

protected:
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	void mouseDoubleClickEvent(QMouseEvent *event);

private:
	void drawLineTo(const QPoint &endPoint);
	void resizeImage(QImage *image, const QSize &newSize);
	int selectKth(int* data, int s, int e, int k);

	bool modified;
	bool painting;
	int myPenWidth;
	QColor myPenColor;
	QImage image;
	QPoint lastPoint;
	int velkost = 0;
	vector<BOD> bodElipsy;
	vector<TROJUHOLNIK> bodTrojuholnika;
	int xs; 
	int ys;
	QVector<QPointF> body;
};

#endif // PAINTWIDGET_H
