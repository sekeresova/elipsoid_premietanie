#include "paintwidget.h"


PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 5;
	myPenColor = Qt::black;
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		painting = true;
		body.push_back(lastPoint);
		printf("surX %lf\n",body[body.size() - 1].x());
		printf("surY %lf\n", body[body.size() - 1].y());
		painter.drawPoint(body[body.size() - 1]);
	}
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::LeftButton) && painting)
		drawLineTo(event->pos());
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && painting) {
		drawLineTo(event->pos());
		painting = false;
	}
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.drawLine(lastPoint, endPoint);
	modified = true;

	int rad = (myPenWidth / 2) + 2;
	update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	lastPoint = endPoint;
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

void PaintWidget::DDA(int x1, int y1, int x2, int y2)
{
	QPainter painter(&image);
	painter.setPen(QPen(Qt::black , myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(Qt::black));

	float  m, xx, yy, deltaY, deltaX, X, Y;
	deltaX = x2 - x1;
	deltaY = y2 - y1;
	if (fabs(deltaY) <= fabs(deltaX)) {
		m = (deltaY / deltaX);
		if (x1 < x2) { xx = x1; yy = y1; X = x2; }
		else { xx = x2; yy = y2; X = x1; }
		painter.drawEllipse((int)xx, (int)yy, 3, 3);
		while (xx < X) {
			xx = xx + 1.0;
			yy = yy + m;
			painter.drawEllipse(xx, (int)yy, 3, 3);
		}
	}
	else {
		m = (deltaX / deltaY);
		if (y1 < y2) { xx = x1; yy = y1; Y = y2; }
		else { xx = x2; yy = y2; Y = y1; }
		painter.drawEllipse((int)xx, (int)yy, 3, 3);
		while (yy < Y) {
			xx = xx + m;
			yy = yy + 1.0;
			painter.drawEllipse((int)xx, yy, 3, 3);
		}
	}
	update();
}

void PaintWidget::elipsoid(int rovnobezky, int poludniky, int a, int b, int c)
{
	QPainter painter(&image);
	float posun_v = (2 * M_PI) / poludniky;		// poma == posun_v 
	float posun_t = (M_PI) / rovnobezky;		// pomb == posun_t 
	float v = 0;
	float t = 0;
	xs = image.width();
	ys = image.height();
	rovnobezky++;
	BOD bod;
	TROJUHOLNIK troj;

	for (int i = 0; i <= rovnobezky; i++) {
		v = 0;
		for (int j = 0; j < poludniky; j++) {
			bod.x = a*cos(v)*sin(t);
			bod.y = b*sin(v)*sin(t);
			bod.z = c*cos(v);

			bodElipsy.push_back(bod);
			velkost++;
			v += posun_v;
		}
		t += posun_t;
	}

	printf("velkost bodElipsy: %d\n", int(bodElipsy.size()));
	printf("velkost premenenna: %d\n\n", velkost);
	for (int i = 0; i < bodElipsy.size(); i++) {
		printf("bod %d a surX: %lf \n", i, bodElipsy[i].x);
		printf("bod %d a surY: %lf \n", i, bodElipsy[i].y);
		painter.drawEllipse((xs / 2) + bodElipsy[i].x, (ys / 2) + bodElipsy[i].y, 5, 5);
	}

	int pocet_poly = ((rovnobezky * poludniky) - poludniky) * 2;

	rovnobezky--;
	for (int i = poludniky; i < poludniky * 2; i++)
	{
		if (i == (poludniky * 2) - 1) {
			troj.a = poludniky;
		}
		else {
			troj.a = i + 1;
		}
		troj.b = i;
		troj.c = 0;
		bodTrojuholnika.push_back(troj);
		printf("som v prvom FORE %d-krat\n", i);
	}

	printf("\n\npocet bodov je: %d\n", pocet_poly);

	for (int i = poludniky; i < (pocet_poly / 2); i++)
	{
		if (i % poludniky == poludniky - 1)
		{	
			i -= poludniky;
			troj.a = i + poludniky;
			troj.b = i + 1;
			troj.c = i + 2 * poludniky;
			bodTrojuholnika.push_back(troj);

			troj.a = i + 1;
			troj.b = i + poludniky + 1;
			troj.c = i + 2 * poludniky;
			bodTrojuholnika.push_back(troj);
			i += poludniky;
		}
		else
		{
			troj.a = i;
			troj.b = i + 1;
			troj.c = i + poludniky;
			bodTrojuholnika.push_back(troj);

			troj.a = i + 1;
			troj.b = i + poludniky + 1;
			troj.c = i + poludniky;
			bodTrojuholnika.push_back(troj);
		}
		printf("som v DRUHOM FORE %d-krat\n", i);
	}

	for (int i = pocet_poly / 2; i < poludniky*(rovnobezky + 1); i++)
	{
		if (i == (poludniky*(rovnobezky + 1)) - 1) {
			troj.a = i;
			troj.b = pocet_poly / 2;
			troj.c = poludniky*(rovnobezky + 1);
			bodTrojuholnika.push_back(troj);
		}
		else {
			troj.a = i;
			troj.b = i + 1;
			troj.c = poludniky*(rovnobezky + 1);
			bodTrojuholnika.push_back(troj);
		}
		printf("som v TRETOM FORE %d-krat\n", i);
	}
	update();
	printf("body Trojuholnika\n");
	for (int i = 0; i < bodTrojuholnika.size(); i++) {
		printf("\n%d. bod\n", i);
		printf("bodTrojuholnika a: %lf\n", bodTrojuholnika[i].a);
		printf("bodTrojuholnika b: %lf\n", bodTrojuholnika[i].b);
		printf("bodTrojuholnika c: %lf\n", bodTrojuholnika[i].c);
	}
}

void PaintWidget::BezieroveKrivky()
{
	QPainter painter(&image);
	QPointF B;
	int n = body.size();
	QVector<QPointF> body1(n);
	QVector<QPointF> body2(n);
	body1 = body;
	double t = 0;
	double deltaT = 0.0001;
	B.setX(body[0].x());
	B.setY(body[0].y());

	while(t < 1) {
		t = t + deltaT;
		for (int j = 1; j < n; j++) {
			for (int i = 0; i < n - j; i++) {
				body2[i] = (1 - t) * body1[i] + t * body1[i + 1];
			}
			body1 = body2;
		}
		painter.drawLine(B.toPoint(), body1[0].toPoint());
		B = body1[0];
		body1 = body;
	}

	body.clear();
	update();
}
