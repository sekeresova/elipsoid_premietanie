#pragma once
class Bod
{
private:
	double x, y, z;
public:
	Bod();
	~Bod();
	double X() { return x; }
	double Y() { return y; }
	double Z() { return z; }
	void nastav_x(double xx) { this->x = xx; }
	void nastav_y(double yy) { this->y = yy; }
	void nastav_z(double zz) { this->z = zz; }
};

