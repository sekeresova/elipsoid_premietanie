#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
	paintWidget.newImage(800, 800);
}

MyPainter::~MyPainter()
{

}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "image files (*.png *.jpg *.bmp)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);
}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.clearImage();
}

void MyPainter::ActionNew()
{
	paintWidget.newImage(800, 600);
}

void MyPainter::ActionKresli()
{
	vector<BOD> bodElipsy;
	vector<TROJUHOLNIK> bodTrojuholnika;
	int xs, ys, a, b, c, poludniky, rovnobezky;

	poludniky = ui.spinBox_poludniky->value();
	rovnobezky = ui.spinBox_rovnobezky->value();
	a = ui.spinBox_A->value();
	b = ui.spinBox_B->value();
	c = ui.spinBox_C->value();
	paintWidget.elipsoid(rovnobezky, poludniky, a, b, c);
	xs = paintWidget.DajXs();
	ys = paintWidget.DajYs();
	bodElipsy = paintWidget.DajBodElipsy();
	bodTrojuholnika = paintWidget.DajBodTrojuholnika();

	printf("\n\n\nTERAZ SKOPIROVANE BODY!!! \n");
	for (int i = 0; i < bodElipsy.size(); i++) {
		printf("bod %d a surX: %lf \n", i, bodElipsy[i].x);
		printf("bod %d a surY: %lf \n", i, bodElipsy[i].y);
	}

	if (ui.comboBox->currentIndex() == 0) {
		for (int i = 0; i < bodElipsy.size() - 1; i++) {
			paintWidget.DDA((xs/2) + bodElipsy[i].x, (ys/2) + bodElipsy[i].y, (xs/2) + bodElipsy[i + 1].x, (ys/2) + bodElipsy[i + 1].y);
		}
	}

}

void MyPainter::ActionKresliKrivku()
{
	paintWidget.BezieroveKrivky();
}